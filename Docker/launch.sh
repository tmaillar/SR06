#!/bin/bash

docker network rm vlan82 vlan83
docker rm container1 container2

docker network create -d macvlan --subnet=10.0.82.0/24 --gateway=10.0.82.1 -o parent=ens9.82 vlan82
docker network create -d macvlan --subnet=10.0.83.0/24 --gateway=10.0.83.1 -o parent=ens9.83 vlan83


docker run --name=container1 --hostname=container1 --net=vlan82 \
           --ip=10.0.82.3 -p21-22:21-22 -p 80:80 -p 10000-10010:10000-10010 \
           -d server:sr06

docker run --name=container2 --hostname=container2 --net=vlan83 --ip=10.0.83.3 \
           -d client:sr06
