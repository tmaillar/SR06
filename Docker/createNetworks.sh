#!/bin/bash
docker network create -d macvlan --subnet=10.0.82.0/24 --gateway=10.0.82.1 -o parent=ens9.82 vlan82

docker network create -d macvlan --subnet=10.0.83.0/24 --gateway=10.0.83.1 -o parent=ens9.83 vlan83
